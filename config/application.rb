require_relative 'boot'

require 'rails/all'
require 'iota'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PayPerRequest
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.client = IOTA::Client.new(host: 'https://nodes.devnet.iota.org', port: 443)
    pp "PayPerRequest:: Loading account details..."
    pp "@@client"
    pp "@@client"
    pp "@@client"
    pp "@@client"
    config.account = IOTA::Models::Account.new(config.client, '9SRIVWUAY9KJTDUBVPZFVBBYAYIGPIJHDHANAG9ISVBUBZIFNADCXZVMFLAH9YJEY9MHCLXYVEHETNIIC')
    pp "PayPerRequest:: Loading account details..."
    config.account.getAccountDetails()
    pp "PayPerRequest:: Loading account details done."
    config.balance = config.account.balance

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
