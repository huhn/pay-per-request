class HomeController < ApplicationController
  def index
    @account = PayPerRequest::Application.config.account
    @balance = PayPerRequest::Application.config.balance
  end
end
