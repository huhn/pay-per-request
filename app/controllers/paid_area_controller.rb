class PaidAreaController < ApplicationController

  before_action :do_payment

  def index
      @balance = PayPerRequest::Application.config.balance
  end

  private

  def do_payment

    if PayPerRequest::Application.config.balance > 0.0
      ## good to go
      IotaService.buy_request
    else
      flash[:error] = 'You cant do this, the pot is empty'

      redirect_to root_path
    end

  end
end
