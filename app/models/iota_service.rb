class IotaService

  require 'iota'

  def self.buy_request
    transfer = IOTA::Models::Transfer.new(
        address: 'EVLVJYD9BDMKJTEWELUCGLMNQSIBAAWWLVGZCFRVLVPDUPLCUVDOVHXJ9NTFWNIZ9DKCZFOOGZPMXPTLBWETFZTFLX',
        value: 1
    )
    depth = 3
    minWeightMagnitude = 9
    PayPerRequest::Application.config.account.sendTransfer(depth, minWeightMagnitude, [transfer], {})
    PayPerRequest::Application.config.balance = PayPerRequest::Application.config.balance - 1
  end

end
